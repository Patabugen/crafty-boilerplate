This is a boilerplate for Crafty which includes:

* Bower package management
* Grunt Build Process
* NPM package management (required for Grunt)

# Philosophy
I wanted an easy way to distribute my [Crafty Game Engine](http://craftyjs.com/) games
which has a proper build process to make them easy to distribute.

Inspired by the [Angular JS Boilerplate](https://github.com/ngbp/ngbp), I created
a similar one for Crafty, check out that project for an idea of how cool I'd like
to make this.

# Installation

`these bits` should be run from the command line in the directory where you checked out this repo.

1. Clone this repo
1. Update `package.json` and `bower.json` with your projects name
1. If you don't have Node & NPM installed: http://nodejs.org/
1. If you don't have Grunt installed: `npm install -g grunt-cli`
1. If you don't have Bower installed: `npm install -g bower`
1. `bower install` to install the JS dependencies
1. `npm install` to install the Grunt dependencies
1. `grunt` to run a build, and check everything works. If you get errors you can't debug, post an issue on the public repository.

You should now be all set to start development, before you begin each session
run `grunt watch` to automatically run a build as you edit your files.

# Getting Strted
After you've read this readme and understand what all the various files do make sure
you can run Grunt. Once that's all working, head into `src/Game.js` to setup your
basic config and start creating scenes in `/src/scenes`.

Also read the comments in `/src/index.html` for more information on how the game is bootstrapped.

# Build Process
There are two options for building, `build_dev` and `build`, the latter uglifies
Javscript files while the former doesn't.

# File System
An explanation of the directories involved:

* `/src/` Holds all your game-specific files
* `/public/` Created by the build process and filled with distributable files. **Warning: This directory gets deleted when the package is built, so don't store or change things here!**
* `/src/audio/` for storing your audio files
* `/src/css/` in case you wish to include any css files.
* `/src/data/` A place for your project data files, for example editable PSD files or other sources used to build assets.
* `/src/img/` For all your images
* `/src/js/` All your Javascript files go here, see below for more.
* `/src/lib/` external libraries (like Crafty and Zepto) go hee, bower is configured to put it's files here.
* `/src/index.html` Your games Index, loads everything else.

Some other files:

* `/.bowerrc` configures Bower Package Management, all I've set here is where to unpack libraries to.
* `/.gitignore` Tells Git to ignore the `public` and `node_modules` directory.
* `/bower.json` Project information for Bower 
* `/Gruntfile.js` Build configuration for Grunt 
* `/package.json` Project information for NPM (also used by Grunt) 
* `/readme.md` This file, which hopefully introduces you to the whole project 

# Todo:
* Allow for using LESS and processing any CSS files
* Adding LiveReload support.
* Give the Loading screen some content (i.e log the Loding progress on to it);
* Move the Sprite definitions out of Loading.js
* Possibly put the Asset and Audio Assset definitions into a module of it's own (rather than having them in `/game.js`)
* Having two files named `game.js` is confusing to document, lets rename one of them!
