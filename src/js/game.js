/**
* This file created a global variable called Game which contains any settings
* or utility functions you'd like. It also contains list of assets which are loaded
* by the Loading scene before switching to the Game scene.
* The method here called load() is called from index.html and some settings are
* passed in (you could, for example, pass in game options from your webpage)
**/
(function(){
	window.Game = {
		// Most games will want these
		width: 800,
		height: 600,
		baseUrl: '',

		// Misc game-specific settings/stores. These are for your own use, and not
		// automatically processed.
		language: 'en-gb',
		speed: 10,

		// Helper functions for getting the centre of the viewport
		centerX: function(){ return Math.round(this.width / 2); },
		centerY: function(){ return Math.round(this.height / 2); },

		// A handy place to store your controls for later reference (again these
		// are not used automatically by anything)
		controls: {
			pause: Crafty.keys.P,
			P1: {
				up: Crafty.keys.UP_ARROW,
				down: Crafty.keys.DOWN_ARROW,
				fire: Crafty.keys.LEFT_ARROW
			},
			P2: {
				up: Crafty.keys.W,
				down: Crafty.keys.S,
				fire: Crafty.keys.D
			}
		},

		// List here any assets (usually image files, I think) you wish the loader
		// to load. Add these in load() if you want to be able to use this.baseUrl
		assets: [],

		// List any audio files you'd like loaded (by the loader). The property
		// name is the audio name:
		// 		audio: { 'shoot' : this.baseUrl + '/audio/shoot' + audioExtension }
		// might become
		//		Crafty.audio.add('shoot', 'mygame/audio/shoot.ogg');
		// Add these in load() if you want to be able to use this.baseUrl
		audio: {},

		load: function(options) {
			this.baseUrl = options.baseUrl;
			this.audioExtension = options.audioExtension;

			// Register the assets we want
//			this.assets.push(this.baseUrl + '/img/backgrounds.png');


			// Register any audio elements we want to load
// 			this.audio.shoot = this.baseUrl + '/audio/shoot.' this.audioExtension;

			Crafty.scene("Loading");
		},

		log: function(level, message) {
			if (arguments.length == 1) {
				console.log(level);
			} else {
				console.log(level, message);
			}
		}
	};
}());
